var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var View = laya.ui.View;
var Dialog = laya.ui.Dialog;
var ui;
(function (ui) {
    var MainUI = /** @class */ (function (_super) {
        __extends(MainUI, _super);
        function MainUI() {
            return _super.call(this) || this;
        }
        MainUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.MainUI.uiView);
        };
        MainUI.uiView = { "type": "View", "props": { "width": 720, "height": 1280 }, "child": [{ "type": "Button", "props": { "y": 1182, "x": 25, "width": 245, "var": "playBtn", "skin": "comp/button.png", "sizeGrid": "0,0,0,0", "labelStrokeColor": "#f6f6f6", "labelSize": 28, "label": "开始播放", "height": 69 } }, { "type": "Button", "props": { "y": 1097, "x": 28, "width": 245, "var": "stopBtn", "skin": "comp/button.png", "sizeGrid": "0,0,0,0", "labelStrokeColor": "#f6f6f6", "labelSize": 28, "label": "停止播放", "height": 69 } }] };
        return MainUI;
    }(View));
    ui.MainUI = MainUI;
})(ui || (ui = {}));
//# sourceMappingURL=layaUI.max.all.js.map