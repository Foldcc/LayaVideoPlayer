var VedioPlayer = function(_src, _width, _height, _callback, _caller) {
	this.src = _src;
	this.width = _width;
	this.height = _height;
	this.callback = _callback;
	this.caller = _caller;
	CALLBACK = _callback;
	CALLER = _caller;
	this.mp4player = new MP4Player(new Stream(this.src), true, false, true);
	this.mp4player.avc.onPictureDecoded = function() {
		_callback.call(_caller,arguments[0], arguments[1], arguments[2]);
	};
};
;(function(){
	
	VedioPlayer.prototype.PlayVedio = function() {
		this.mp4player.play();
	};
	VedioPlayer.prototype.StopVedio = function() {
		this.mp4player.pause();
	};
})();
	