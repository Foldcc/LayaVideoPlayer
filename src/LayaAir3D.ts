// 程序入口
class LayaAir3D {
    material: Laya.StandardMaterial;
    player : VedioPlayer;
    isPlayfrist : boolean;
    box: Laya.MeshSprite3D;
    constructor() {
        //初始化引擎
        Laya3D.init(720, 1280, true);

        //适配模式
        Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_AUTO;
        Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;

        //开启统计信息
        Laya.Stat.show();

        Laya.loader.load("res/atlas/comp.atlas" , Laya.Handler.create(this , this.PlayerInit) ,null , Laya.Loader.ATLAS);
    }
       
    PlayerInit(){

         //添加3D场景
        var scene: Laya.Scene = Laya.stage.addChild(new Laya.Scene()) as Laya.Scene;

        //添加照相机
        var camera: Laya.Camera = (scene.addChild(new Laya.Camera(0, 0.1, 100))) as Laya.Camera;
        camera.transform.translate(new Laya.Vector3(0, 2, 3));
        camera.transform.rotate(new Laya.Vector3(-30, 0, 0), true, false);
        camera.clearColor = null;

        //添加方向光
        var directionLight: Laya.DirectionLight = scene.addChild(new Laya.DirectionLight()) as Laya.DirectionLight;
        directionLight.color = new Laya.Vector3(0.6, 0.6, 0.6);
        directionLight.direction = new Laya.Vector3(1, -1, 0);

        //添加自定义模型
        this.box = scene.addChild(new Laya.MeshSprite3D(new Laya.BoxMesh(1, 1, 1))) as Laya.MeshSprite3D;
        this.box.transform.rotate(new Laya.Vector3(0, 45, 0), false, false);
        this.material = new Laya.StandardMaterial();
        this.material.diffuseTexture = Laya.Texture2D.load("res/Texture.png");
        this.box.meshRender.material = this.material;

        

        var mainui = new MainView();
        // mainui.playBtn.on(Laya.Event.MOUSE_DOWN , this , this.playVedio);
        mainui.playBtn.clickHandler = Laya.Handler.create(this , this.playVedio , null , false);
        mainui.stopBtn.clickHandler = Laya.Handler.create(this , this.stopVideo , null , false);
        Laya.timer.loop(1 , this , this.rotationCobe);
        Laya.stage.addChild(mainui);
    }

    rotationCobe(){
        // this.box.transform.rotate(Laya.Vector3.Up);
        this.box.transform.rotate(new Laya.Vector3(0.02,0.01,0));
    }

    playVedio(){
        this.player = new VedioPlayer(
            "res/test512.mp4" ,
             512 , 
             512 , 
             this.updateTexture, 
            this
        );
        this.player.PlayVedio();
    }

    stopVideo(){
        this.player.StopVedio();
    }

    updateTexture(data , width , height){
         alert(width + "  ,  " + height);
         this.VideoCutter(data , width , height , 512 , 512);
         //Laya.DataTexture2D.destroyUnusedResources()
    }

    VideoCutter(data , width , height , cWidth , cHeight){
        if(cWidth > width || cHeight > height) {
            console.error("cwidth or cheight greater than width or height");
            return;
        }
        if( this.material.diffuseTexture){
            this.material.diffuseTexture.releaseResource();
         }
        let Data2 : Uint8Array = new Uint8Array(cWidth*cHeight*4);
         let h , w ;
         h = 0;
         w = 0;
         while (h < cHeight) {
             while(w < width * 4){
                if(w < cWidth * 4){
                    Data2[h*cWidth*4 + w] = data[h * width * 4 + w];
                }
                w++;
             }
             h ++;
             w = 0;
         }
         this.material.diffuseTexture = Laya.DataTexture2D.create(Data2 , cWidth , cHeight);
    }
}
new LayaAir3D();