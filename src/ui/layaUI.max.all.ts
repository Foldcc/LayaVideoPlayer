
import View=laya.ui.View;
import Dialog=laya.ui.Dialog;
module ui {
    export class MainUI extends View {
		public playBtn:Laya.Button;
		public stopBtn:Laya.Button;

        public static  uiView:any ={"type":"View","props":{"width":720,"height":1280},"child":[{"type":"Button","props":{"y":1182,"x":25,"width":245,"var":"playBtn","skin":"comp/button.png","sizeGrid":"0,0,0,0","labelStrokeColor":"#f6f6f6","labelSize":28,"label":"开始播放","height":69}},{"type":"Button","props":{"y":1097,"x":28,"width":245,"var":"stopBtn","skin":"comp/button.png","sizeGrid":"0,0,0,0","labelStrokeColor":"#f6f6f6","labelSize":28,"label":"停止播放","height":69}}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.MainUI.uiView);

        }

    }
}
